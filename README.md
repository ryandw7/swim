== swimstroke ==
This Python library is for parsing files containing competitive swimming data: list of events and qualifying times, lists of entries for events, and list of results. Typically, this would be used to consume data exported from a software package like Hy-Tek's Team Manager or Meet Manager (although this Python package is not affiliated with Hy-Tek in any way).

This library parses .hy3, .ev3, .scb (scoreboard) files. It also consumes .yml files so you can produce your own swim data more easily.

There are other software packages for parsing this swim data. The objective for this package is to parse the imported files into a standard Python structure that is easy to query and manipulate. And to only use Python dicts/arrays instead of full classes and objects (so that it can be easily pushed into a NoSQL database or sent via JSON to a web client without any special serialization).

Where possible, this package strives to treat times as milliseconds. Most swimming times are only captured to the hundreths of a second; this package deviates and uses milliseconds because many Python and Javascript APIs work in milliseconds. Dates will be converted to ISO formats (e.g. YYYY-MM-DD) but as strings, not date objects. Internal identifiers (e.g. swimmer codes) will not be exported and swimmer IDs (which are more universally unique and used across multiple files) will be used where possible.

meetinfo
- name
- location
- startdate : str (YYYY-MM-DD)
- enddate : str (YYYY-MM-DD)
- qualifying_age_date : str (YYYY-MM-DD)
- qualifying_date_from : str (YYYY-MM-DD)
- qualifying_date_to : str (YYYY-MM-DD)
- events[]
  - index : str (usually the event number, but sometimes events have letters also)
  - name
  - date
  - gender
  - gendercode
  - course
  - coursecode
  - stroke
  - strokeshort
  - distance
  - relay : bool
  - type
  - num_heats
  - eligibility[]
    - age_from
    - age_to
    - course
    - gender
    - gendercode
    - time_ms
- teams[]
  - name
  - short_name
- swimmers[]
  - swimmer_id
  - lastname
  - firstname
  - preferredname
  - middlei
  - gender
  - birthday : str (YYYY-MM-DD)
  - age
  - team_short_name
- entries[]
  - event_index (note that swimmers can have multiple entries for the same event if these are results in prelim/finals type setup)
  - heat
  - heat_number
  - lane
  - event_gendercode
  - event_gender
  - event_course
  - event_coursecode
  - stroke
  - strokeshort
  - distance
  - seed_time
  - seed_course
  - seed_coursecode
  - seed_time_ms
  - seed_time_str ?
  - relay : bool
  - teamname : str (for relays)
  - swimmer_ids[] : str
  - event_type
  - event_date
  - result_time_ms
  - place
  - dq : bool