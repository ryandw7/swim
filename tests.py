import unittest, urllib.request, os.path


class SwimmingNZ_HY3_Test(unittest.TestCase):
    def test_2022_nz_swimming_championships_basic_parse(self):
        """
        See if we can parse basic information from this publicly available dataset
        """
        import swimstroke
        meetinfo = swimstroke.load("https://75f86887-d0ad-4409-9253-2e11c6209267.filesusr.com/archives/6f2a10_9705a6fcb3144ad0aab00d3a523a0bb2.zip?dn=Meet%20Results%202022%20Apollo%20Projects%20NZ%20Swimming%20Championships%202022_04_04.zip")
        events = meetinfo['events']

        self.assertEqual("2022 Apollo Projects NZ Swimming Championship", meetinfo['name'])
        self.assertEqual(43, len(meetinfo['teams']))

        self.assertEqual("Capital Swim Club",meetinfo['teams'][2]['name'])

        self.assertEqual(6, len([s for s in meetinfo['swimmers'] if s['team_short_name']=="CAPWN"]))

        # test some extracted values from swimmers
        self.assertEqual("CAPRJR150204", meetinfo['swimmers'][12]['swimmer_id'])
        self.assertEqual(18, meetinfo['swimmers'][12]['age'])

        # test some entries
        ENTRY_INDEX = 78
        self.assertEqual("11", meetinfo['entries'][ENTRY_INDEX]['event_index'])
        self.assertEqual("F", meetinfo['entries'][ENTRY_INDEX]['event_gendercode'])
        self.assertEqual(66410, meetinfo['entries'][ENTRY_INDEX]['seed_time_ms'])
        self.assertEqual(67850, meetinfo['entries'][ENTRY_INDEX]['result_time_ms'])
        self.assertEqual(17, meetinfo['entries'][ENTRY_INDEX]['place'])
        self.assertEqual(100, meetinfo['entries'][ENTRY_INDEX]['distance'])
        self.assertEqual('Backstroke', meetinfo['entries'][ENTRY_INDEX]['stroke'])
        self.assertEqual('3', meetinfo['entries'][ENTRY_INDEX]['heat'])
        self.assertEqual('1', meetinfo['entries'][ENTRY_INDEX]['lane'])
        self.assertEqual(False, meetinfo['entries'][ENTRY_INDEX]['dq'])

        self.assertEqual('LCM', meetinfo['entries'][9]['event_course'])

        self.assertEqual(True, meetinfo['entries'][20]['dq'])
        self.assertEqual(40760, meetinfo['entries'][20]['result_time_ms'])

        # test result format
        self.assertEqual("1:07.85L", swimstroke.util.format_result_time(meetinfo['entries'][ENTRY_INDEX]))

    def test_2022_nz_swimming_championships_get_events(self):
        """
        Test populate_events_entries helper which merges meetinfo results together
        """
        import swimstroke
        meetinfo = swimstroke.load("https://75f86887-d0ad-4409-9253-2e11c6209267.filesusr.com/archives/6f2a10_9705a6fcb3144ad0aab00d3a523a0bb2.zip?dn=Meet%20Results%202022%20Apollo%20Projects%20NZ%20Swimming%20Championships%202022_04_04.zip")
        events = meetinfo['events']
        swimstroke.populate_events_entries(meetinfo)

        self.assertEqual("5", events[2]['index']) # events are currently sorted with prelims first then finals. So 5 is in event 3's position
        self.assertEqual('LCM', events[2]['course'])
        self.assertEqual('Individual Medley', events[2]['stroke'])
        self.assertEqual(200, events[2]['distance'])
        self.assertEqual(4, events[2]['num_heats'])
        self.assertEqual(27, len(events[2]['entries']))
        self.assertEqual("Prelim", events[2]['type'])

    def test_2023_wellington_distance_championships(self):
        import swimstroke
        from swimstroke.util import swimtimefmt

        meetinfo = swimstroke.load("https://www.swimmingnz.org/_files/archives/81a1e6_b6a2df7fefb34f37a45e1ecd9b3c4bd7.zip?dn=Meet%20Results-Swim%20Wellington%20Distance%20Meet%202023-26Aug2023-001.zip")

        self.assertEqual(58, len(meetinfo['entries']))
        self.assertEqual("16:47.73", swimtimefmt(meetinfo['entries'][0]['seed_time_ms']))
        self.assertEqual("21:01.70", swimtimefmt(meetinfo['entries'][2]['result_time_ms']))
        self.assertEqual("M", meetinfo['swimmers'][0]['gender'])
        self.assertEqual("F", meetinfo['swimmers'][2]['gender'])

    def test_2023_wellington_relays(self):
        # This meet had some weird sized relays! Useful for testing.
        import swimstroke
        meetinfo = swimstroke.load("https://www.swimmingnz.org/_files/archives/81a1e6_58a527a0c7044bbc9a61f8bc430f7793.zip?dn=Meet%20Results-SW%20Relay%20Champs%202023-16Sep2023-001.zip")

        swimmer_ids = set()
        for s in meetinfo['swimmers']:
            if s['swimmer_id'] in swimmer_ids:
                self.fail("duplicate swimmer id! "+s['swimmer_id'])
            swimmer_ids.add(s['swimmer_id'])

        self.assertEqual(5, len(meetinfo['entries'][0]['swimmer_ids']))

    def test_2024_wellington_short_course_champs(self):
        import swimstroke
        meetinfo = swimstroke.load("https://www.swimmingnz.org/_files/archives/ec0271_e1b3cdce829a41a6a197922cfd2caba0.zip?dn=Meet%20Results-Wellington%20Short%20Course%20Championship%202024-22Jun2024-003.zip")

        self.assertEqual(5, meetinfo['entries'][1]['points'])
        self.assertEqual([
            (50,32690),
            (100,69800),
            (150,108020),
            (200,147120),
            (250,186090),
            (300,225980),
            (350,265990),
            (400,304820),
        ],
            meetinfo['entries'][7]['splits']
        )
        self.assertEqual(304820,meetinfo['entries'][7]['result_time_ms'])

    def test_swimstrokemeet1_results(self):
        ## all fake data

        import swimstroke
        from swimstroke.util import swimtimefmt

        meetinfo = swimstroke.load(os.path.join(".","testdata","Meet Results-Swimstroke Meet 1-01Jan2024-001.zip"))
        events = meetinfo['events']

        swimstroke.populate_events_entries(meetinfo)
        for e in events:
            swimstroke.populate_heats(e)
        
        self.assertEqual("18:20.03", swimtimefmt(events[3]['entries'][0]['seed_time_ms']))
        self.assertEqual("18:10.46", swimtimefmt(events[3]['entries'][0]['result_time_ms']))
        self.assertEqual("Reynolds", events[3]['entries'][0]['swimmers'][0]['lastname'])
        self.assertEqual("Stephanie", events[3]['entries'][0]['swimmers'][0]['firstname'])
        self.assertEqual("2004-11-14", events[3]['entries'][0]['swimmers'][0]['birthday'])
        self.assertEqual(1090460, events[3]['entries'][0]['result_time_ms'])
        self.assertEqual(1500, events[3]['distance'])
        self.assertEqual("Free", events[3]['strokeshort'])
        self.assertEqual("2", events[3]['entries'][0]['lane'])
        self.assertEqual("1", events[3]['entries'][0]['heat'])

        # Make sure that the entry on the event equals the entry on the heat (test of populate_heats)
        self.assertEqual(events[3]['entries'][0], events[3]['heats'][0]['entries'][0])

    def test_swimstrokemeet1_get_lanes(self):
        ## all fake data

        import swimstroke
        meetinfo = swimstroke.load(os.path.join(".","testdata","Meet Results-Swimstroke Meet 1-01Jan2024-001.zip"))

        lanes = swimstroke.get_lanes(meetinfo)
        self.assertEqual(['1','2','3','4'],lanes)

class swimtimeparser(unittest.TestCase):
    def test_strtoms(self):
        from swimstroke.util import swimtime_strtoms

        self.assertEqual(swimtime_strtoms("4:58.00"),298000)
        self.assertEqual(swimtime_strtoms("3.06"),3060)
        self.assertEqual(swimtime_strtoms("3.6"),3600)
        self.assertEqual(swimtime_strtoms("31"),31000)
        self.assertEqual(swimtime_strtoms("1:02.40"),62400)
        self.assertEqual(swimtime_strtoms(""),None)

    def test_msfmt(self):
        from swimstroke.util import swimtimefmt

        self.assertEqual(swimtimefmt(298000),"4:58.00")
        self.assertEqual(swimtimefmt(3060),"3.06")
        self.assertEqual(swimtimefmt(None),"")

class SCB_Parser_Tests(unittest.TestCase):
    def test_SwimStroke1Meet(self):
        import swimstroke
        meetinfo = swimstroke.load(os.path.join(".","testdata","SCB_Swimstroke Meet 1.zip"))

        swimstroke.populate_events_entries(meetinfo)
        swimstroke.populate_heats(meetinfo['events'][0])

        events = meetinfo['events']

        self.assertEqual(6, len(events))
        self.assertEqual(100, events[0]['distance'])
        self.assertEqual("Mixed", events[0]['gender'])
        #print(events[0])
        self.assertEqual("DOE, JOHN", events[0]['heats'][0]['entries'][0]['swimmers'][0]['name'])
        self.assertEqual(1, events[0]['heats'][0]['entries'][0]['lane'])
        self.assertEqual("DOE, JANE", events[0]['heats'][0]['entries'][3]['swimmers'][0]['name'])
        self.assertEqual(4, events[0]['heats'][0]['entries'][3]['lane'])

class EV3_Parser_Tests(unittest.TestCase):
    def test_swimstrokemeet1(self):
        import swimstroke
        meetinfo = swimstroke.load(os.path.join(".","testdata","Meet Events-Swimstroke Meet 1-01Jan2024-001.zip"))
        # just test parsing for now

    def test_2023_nz_div2(self):
        self.maxDiff = None

        import swimstroke
        meetinfo = swimstroke.load(os.path.join(".","testdata","Meet Events-2023 Division II Swimming Competition-16May2023-002.zip"))

        self.assertEqual(meetinfo['qualifying_age_date'],"2023-04-30")
        self.assertEqual(meetinfo['qualifying_date_from'],"2022-01-01")
        self.assertEqual(meetinfo['qualifying_date_to'],"2023-05-03")

        self.assertEqual(len(meetinfo['events']),44) # 44 events not including para events

        self.assertEqual(meetinfo['events'][1]['index'],"2")
        self.assertEqual(meetinfo['events'][1]['course'],"SCM")
        self.assertEqual(meetinfo['events'][1]['distance'],50)
        self.assertEqual(meetinfo['events'][1]['stroke'],"Breaststroke")

        self.assertEqual(meetinfo['events'][1]['eligibility'],[
            {"age_from":13, "age_to":13, "gender":"Women", "gendercode":"G", "time_ms":40000, "course":"SCM"},
            {"age_from":14, "age_to":14, "gender":"Women", "gendercode":"G", "time_ms":39900, "course":"SCM"},
            {"age_from":15, "age_to":15, "gender":"Women", "gendercode":"G", "time_ms":39800, "course":"SCM"},
            {"age_from":16, "age_to":16, "gender":"Women", "gendercode":"G", "time_ms":39500, "course":"SCM"},
            {"age_from":17, "age_to":18, "gender":"Women", "gendercode":"G", "time_ms":39500, "course":"SCM"},
        ])

        self.assertEqual(meetinfo['events'][8]['index'],"9")
        self.assertEqual(meetinfo['events'][8]['course'],"SCM")
        self.assertEqual(meetinfo['events'][8]['distance'],1500)
        self.assertEqual(meetinfo['events'][8]['stroke'],"Freestyle")

        self.assertEqual(meetinfo['events'][8]['eligibility'],[
            {"age_from":13, "age_to":13, "gender":"Men", "gendercode":"B", "time_ms":1170000, "course":"SCM"},
            {"age_from":14, "age_to":14, "gender":"Men", "gendercode":"B", "time_ms":1150000, "course":"SCM"},
            {"age_from":15, "age_to":15, "gender":"Men", "gendercode":"B", "time_ms":1140000, "course":"SCM"},
            {"age_from":16, "age_to":16, "gender":"Men", "gendercode":"B", "time_ms":1130000, "course":"SCM"},
            {"age_from":17, "age_to":18, "gender":"Men", "gendercode":"B", "time_ms":1130000, "course":"SCM"},
        ])

    def test_2023_nz_nags(self):
        import swimstroke
        meetinfo = swimstroke.load(os.path.join(".","testdata","Meet Events-2023 NZ Age Group Swimming Championships-12Apr2023-004.zip"))

        self.assertEqual(meetinfo['qualifying_age_date'],"2023-04-30")
        self.assertEqual(meetinfo['qualifying_date_from'],"2022-01-01")
        self.assertEqual(meetinfo['qualifying_date_to'],"2023-03-29")

        self.assertEqual(len(meetinfo['events']),46) # 46 events not including para events

        self.assertEqual(meetinfo['events'][1]['index'],"2")
        self.assertEqual(meetinfo['events'][1]['course'],"LCM")
        self.assertEqual(meetinfo['events'][1]['distance'],100)
        self.assertEqual(meetinfo['events'][1]['stroke'],"Butterfly")

        self.assertEqual(meetinfo['events'][1]['eligibility'],[
            {"age_from":13, "age_to":13, "gender":"Men", "gendercode":"M", "time_ms":70700, "course":"LCM"},
            {"age_from":14, "age_to":14, "gender":"Men", "gendercode":"M", "time_ms":67200, "course":"LCM"},
            {"age_from":15, "age_to":15, "gender":"Men", "gendercode":"M", "time_ms":65000, "course":"LCM"},
            {"age_from":16, "age_to":16, "gender":"Men", "gendercode":"M", "time_ms":62000, "course":"LCM"},
            {"age_from":17, "age_to":18, "gender":"Men", "gendercode":"M", "time_ms":60800, "course":"LCM"},
        ])

        self.assertEqual(meetinfo['events'][6]['index'],"7")
        self.assertEqual(meetinfo['events'][6]['course'],"LCM")
        self.assertEqual(meetinfo['events'][6]['distance'],50)
        self.assertEqual(meetinfo['events'][6]['stroke'],"Backstroke")

        self.assertEqual(meetinfo['events'][6]['eligibility'],[
            {"age_from":13, "age_to":13, "gender":"Women", "gendercode":"W", "time_ms":34100, "course":"LCM"},
            {"age_from":14, "age_to":14, "gender":"Women", "gendercode":"W", "time_ms":33200, "course":"LCM"},
            {"age_from":15, "age_to":15, "gender":"Women", "gendercode":"W", "time_ms":33000, "course":"LCM"},
            {"age_from":16, "age_to":16, "gender":"Women", "gendercode":"W", "time_ms":32600, "course":"LCM"},
            {"age_from":17, "age_to":18, "gender":"Women", "gendercode":"W", "time_ms":32000, "course":"LCM"},
        ])

class YAML_Parser_Tests(unittest.TestCase):
    def test_swimstrokeyaml(self):
        import swimstroke
        meetinfo = swimstroke.load(os.path.join(".","testdata","Swimstroke YAML Test Meet.yml"))
        # just test parsing for now


if __name__ == "__main__":
    unittest.main()